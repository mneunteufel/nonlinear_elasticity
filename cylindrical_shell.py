# Example (slightly adapted) from:
# Reese. On the equivalence of mixed element formulations and the concept of reduced integration in large deformation problems

#NGSolve version >= 6.2.2008-17-g2ebed44be required
from ngsolve import *
from ngsolve.meshes import MakeHexMesh
from math import pi
from time import time

lam = Parameter(24000) #N/mm**2
mu  = Parameter(6000) #N/mm**2
par = Parameter(0.0)

order    = 2
numsteps = 15

I = Id(3)
n = specialcf.normal(3)
h = specialcf.mesh_size

def NeoHooke(mu, lam, F, Log=False):
    if Log:
        return mu/2*(Trace(F.trans*F)-3) - mu*log(Det(F)) + lam/2*log(Det(F))**2
    else:
        return mu/2*(Trace(F.trans*F)-3) - mu*log(Det(F)) + lam/2*(Det(F)-1)**2
def NeoHookeC(mu, lam, C, Log=False):
    if Log:
        return mu/2*(Trace(C)-3) - mu/2*log(Det(C)) + lam/8*log(Det(C))**2
    else:
        return mu/2*(Trace(C)-3) - mu/2*log(Det(C)) + lam/2*(sqrt(Det(C))-1)**2

def SkewT(v):
    if v.dim == 3:
        return 0.5*CoefficientFunction( (0,-v[2], v[1], v[2], 0, -v[0], -v[1], v[0] ,0), dims=(3,3) )
    else:
        return 0.5*CoefficientFunction((0,-v, v,0), dims=(2,2))
    
def Fu(u):
    return Id(u.dim)+Grad(u)
def Cu(u):
    return Fu(u).trans * Fu(u)



def StandardMethod(mesh, order):
    fes = VectorH1(mesh, order=order, dirichletx="righttop|rightbot", dirichlety="rightbot", dirichletz="rightbot|back")
    gfu = GridFunction(fes)

    u,v = fes.TnT()
    F = Grad(u) + I

    a = BilinearForm(fes, symmetric=True, condense=True)
    a += Variation( NeoHooke(mu,lam,F,Log=True)*dx ).Compile()
    a += Variation( -par*force*u*ds("righttop") ).Compile()

    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set((step+1)/numsteps)
            (err, nit) = solvers.Newton(a, gfu, maxerr=5e-8, dampfactor=0.4, maxit=15, printing=False, inverse="sparsecholesky")

    return { "displacement" : gfu, "err" : err, "nit" : nit }

def TDNNS_F_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="rightbot")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="righttop|rightbot|back")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))
    fesF  = V*Sigma*Gamma*Hyb
    gfu_F = GridFunction(fesF)
        
    u, Psym, Fsym, uh = fesF.TrialFunction()
    Fmat = Fsym + SkewT(curl(u))
    
    aF = BilinearForm(fesF, symmetric=True, condense=True)
    aF += Variation( (NeoHooke(mu, lam, Fmat, Log=True))*dx).Compile()
    aF += Variation( InnerProduct(Fsym-Grad(u)-I,Psym)*dx ).Compile()
    aF += Variation( (u-uh)*n*(Psym*n)*n*dx(element_boundary=True) ).Compile()
    aF += Variation( -par*(force*uh.Trace() + force*u.Trace())*ds("righttop") ).Compile()

    #Set F=I
    gfu_F.components[2].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set((step+1)/numsteps)
            (err, nit) = solvers.Newton(aF, gfu_F, maxerr=5e-8, dampfactor=0.4, maxit=12, printing=False, inverse="sparsecholesky")
        
    return { "displacement" : gfu_F.components[0], "err" : err, "nit" : nit }


def TDNNS_C_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="rightbot")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="righttop|rightbot|back")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))
    fesC  = V*Sigma*Gamma*Hyb
    gfu_C = GridFunction(fesC)
    
    u, sigma, C, uh = fesC.TrialFunction()
    
    aC = BilinearForm(fesC, symmetric=True, condense=True)
    aC += Variation( NeoHookeC(mu,lam,C,Log=True)*dx ).Compile()
    aC += Variation( -InnerProduct(C-Cu(u),sigma)*dx ).Compile()
    aC += Variation( -(u-uh)*n*2*(Fu(u)*sigma*n)*n*dx(element_boundary=True) ).Compile()
    aC += Variation( -par*(force*uh.Trace() + force*u.Trace())*ds("righttop") ).Compile()

    #Set C=I
    gfu_C.components[2].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set((step+1)/numsteps)
            (err, nit) = solvers.Newton(aC, gfu_C, dampfactor=0.3, maxit=12, printing=False, maxerr=5e-8, inverse="sparsecholesky")

    return { "displacement" : gfu_C.components[0], "err" : err, "nit" : nit }


def TDNNS_FC_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="rightbot")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="righttop|rightbot|back")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))
    fesFC  = V*Sigma*Gamma*Hyb*Gamma*Gamma
    gfu_FC = GridFunction(fesFC)
    
    u, Psym, Fsym, uh, C, Sig  = fesFC.TrialFunction()
    Fmat = Fsym + SkewT(curl(u))
    CF = Fmat.trans*Fmat
    
    aFC = BilinearForm(fesFC, symmetric=True, condense=True)
    aFC += Variation( NeoHookeC(mu,lam,C,Log=True)*dx ).Compile()
    aFC += Variation( InnerProduct(C - CF,Sig)*dx ).Compile()
    aFC += Variation( InnerProduct(Fsym-Grad(u)-I,Psym)*dx ).Compile()
    aFC += Variation( (u-uh)*n*(Psym*n)*n*dx(element_boundary=True) ).Compile()
    aFC += Variation( -par*(force*uh.Trace() + force*u.Trace())*ds("righttop") ).Compile()

    #Set F=I, C=I
    gfu_FC.components[2].Set(I)
    gfu_FC.components[4].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set((step+1)/numsteps)
            (err, nit) = solvers.Newton(aFC, gfu_FC, dampfactor=0.3, maxit=12, printing=False, maxerr=5e-8, inverse="sparsecholesky")

    return { "displacement" : gfu_FC.components[0], "err" : err, "nit" : nit }


def Solve(method, mesh, order):
    result = None
    if method == "std":
        try:
            result = StandardMethod(mesh, order)
        except:
            print("Exception in standard method was thrown!")
    elif method == "F":
        try:
            result = TDNNS_F_Method(mesh, order)
        except:
            print("Exception in F method was thrown!")
    elif method == "C":
        try:
            result = TDNNS_C_Method(mesh, order)
        except:
            print("Exception in C method was thrown!")
    elif method == "FC":
        try:
            result = TDNNS_FC_Method(mesh, order)
        except:
            print("Exception in FC method was thrown!")
    else:
        raise Exception("method ", method, "not implemented!")
    return result


results = []


for t in [2, 0.2]:
    print("thickness = ", t)
    L   = 15 #mm
    r_i = 9-t/2 #mm

    if t == 2:
        force = CoefficientFunction( (0,-240,0) ) #N/mm^2
    else:
        force = CoefficientFunction( (0,-2.7,0) ) #N/mm^2

    #point of interest
    P = [0,2*r_i+t, L]

    results.append([])
        
    for (nx,ny,nz) in [ (8,1,4), (16,1,8), (32,1,16)]:
        print("Grid = ", nx, "x", ny, "x", nz)
        mesh = MakeHexMesh(nx=nx, ny=ny, nz=nz, secondorder=True, mapping = lambda x,y,z : (r_i*cos(pi*x+pi/2)+cos(pi*x+pi/2)*t*y,r_i+r_i*sin(pi*x+pi/2)+sin(pi*x+pi/2)*t*y,L*z))
        mesh.ngmesh.SetBCName(0,"righttop")
        mesh.ngmesh.SetBCName(1,"inner")
        mesh.ngmesh.SetBCName(2,"rightbot")
        mesh.ngmesh.SetBCName(3,"outer")
        mesh.ngmesh.SetBCName(4,"back")
        mesh.ngmesh.SetBCName(5,"front")
        Draw(mesh)
        SetHeapSize(1000*1000*1000)

        results[-1].append([])
        
        for method in ["std", "F", "C", "FC"]:
            result = Solve(method,mesh,order=2)
            U = sqrt(Integrate( result["displacement"]*result["displacement"], mesh))
            u_A = result["displacement"](mesh(P[0],P[1],P[2]))
            print("u(a)",method," = ", u_A)
            print("||u||_L2",method," = ", U)
            results[-1][-1].append( (u_A[1], U) )

            Draw(Norm(result["displacement"]), mesh, "abs_u_"+method)
        
        input("Press enter to continue")

print(results)
