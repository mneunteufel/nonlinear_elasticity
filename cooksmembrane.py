# Example from:
# Angoshtari, Shojaei, Yavari. Compatible-Strain Mixed Finite Element Methods for 2D Compressible Nonlinear Elasticity
# Reese. On the equivalence of mixed element formulations and the concept of reduced integration in large deformation problems

from ngsolve import *
from ngsolve.meshes import MakeQuadMesh
SetVisualization(deformation=True)

L_1 = 48 #48m
L_2 = 44 #44m
L_3 = 16 #16m
#point of interest
P = (L_1,L_2+L_3)

mu   = Parameter(80.194) #N/mm^2
lam  = Parameter(400889.8) #N/mm^2

fz    = Parameter(0) #N/mm^2  
force = CoefficientFunction( (0,fz) )
par   = Parameter(0.0)

order    = 2
numsteps = 35

I = Id(2)
n = specialcf.normal(2)
h = specialcf.mesh_size

def NeoHooke(mu,lam,F, Log=False):
    if Log:
        return mu/2*(Trace(F.trans*F)-2) - mu*log(Det(F)) + lam/2*log(Det(F))**2
    else:
        return mu/2*(Trace(F.trans*F)-2) - mu*log(Det(F)) + lam/2*(Det(F)-1)**2
def NeoHookeC(mu, lam, C, Log=False):
    if Log:
        return mu/2*(Trace(C)-2) - mu/2*log(Det(C)) + lam/8*log(Det(C))**2
    else:
        return mu/2*(Trace(C)-2) - mu/2*log(Det(C)) + lam/2*(sqrt(Det(C))-1)**2

def SkewT(v):
    if v.dim == 3:
        return 0.5*CoefficientFunction( (0,-v[2], v[1], v[2], 0, -v[0], -v[1], v[0] ,0), dims=(3,3) )
    else:
        return 0.5*CoefficientFunction((0,-v, v,0), dims=(2,2))

def Fu(u):
    return Id(u.dim)+Grad(u)
def Cu(u):
    return Fu(u).trans * Fu(u)


def StandardMethod(mesh, order):
    fes = VectorH1(mesh, order=order, dirichlet="left")
    gfu = GridFunction(fes)

    u = fes.TrialFunction()
    F = Grad(u) + I

    a = BilinearForm(fes, symmetric=True, condense=True)
    a += Variation( NeoHooke(mu,lam,F,Log=True)*dx ).Compile()
    a += Variation( -par*force*u*ds("right") ).Compile()

    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set( (step+1)/numsteps )
            (err, nit) = solvers.Newton(a, gfu, maxerr=5e-8, dampfactor=0.2, maxit=15, printing=False, inverse="sparsecholesky")

    return { "displacement" : gfu, "err" : err, "nit" : nit }


def TDNNS_F_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="left")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="left")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))
    fesF  = V*Sigma*Gamma*Hyb
    gfu_F = GridFunction(fesF)
        
    u, Psym, Fsym, uh = fesF.TrialFunction()
    Fmat = Fsym + SkewT(curl(u))

    aF = BilinearForm(fesF, symmetric=True, condense=True)
    aF += Variation( NeoHooke(mu,lam,Fmat,Log=True)*dx ).Compile()
    aF += Variation( InnerProduct(Fsym-Grad(u)-I,Psym)*dx ).Compile()
    aF += Variation( (u-uh)*n*((Psym*n)*n)*dx(element_boundary=True) ).Compile()
    aF += Variation( -par*force*u.Trace()*ds("right") )

    #Set F=I
    gfu_F.components[2].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set( (step+1)/numsteps )
            (err, nit) = solvers.Newton(aF, gfu_F, dampfactor=0.1, maxit=20, printing=False, maxerr=5e-8, inverse="sparsecholesky")

    return { "displacement" : gfu_F.components[0], "err" : err, "nit" : nit }

def TDNNS_C_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="left")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="left")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))    
    fesC  = V*Sigma*Gamma*Hyb
    gfu_C = GridFunction(fesC)
    
    u, sigma, C, uh = fesC.TrialFunction()
    
    aC = BilinearForm(fesC, symmetric=True, condense=True)
    aC += Variation( NeoHookeC(mu,lam,C,Log=True)*dx ).Compile()
    aC += Variation( -InnerProduct(C-Cu(u),sigma)*dx ).Compile()
    aC += Variation( -(u-uh)*n*2*(Fu(u)*sigma*n)*n*dx(element_boundary=True) ).Compile()
    aC += Variation( -par*force*u.Trace()*ds("right") )
    #Stability
    aC += Variation( mu/2*InnerProduct(C-Cu(u), C-Cu(u))*dx ).Compile()
    aC += Variation( mu/2*1/h*((u-uh)*n)**2*dx(element_boundary=True) )

    #Set C=I
    gfu_C.components[2].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set( (step+1)/numsteps )
            (err, nit) = solvers.Newton(aC, gfu_C, dampfactor=0.1, maxit=20, printing=False, maxerr=5e-8, inverse="sparsecholesky")

    return { "displacement" : gfu_C.components[0], "err" : err, "nit" : nit }


def TDNNS_FC_Method(mesh, order):
    V     = HCurl(mesh, order=order, dirichlet="left")
    Sigma = Discontinuous(HDivDiv(mesh, order=order))
    Hyb   = NormalFacetFESpace(mesh, order=order, dirichlet="left")
    Gamma = Discontinuous(HCurlCurl(mesh, order=order+1))
    fesFC  = V*Sigma*Gamma*Hyb*Gamma*Gamma
    gfu_FC = GridFunction(fesFC)
    
    u, Psym, Fsym, uh, C, Sig  = fesFC.TrialFunction()
    Fmat = Fsym + SkewT(curl(u))
    CF = Fmat.trans*Fmat
    
    aFC = BilinearForm(fesFC, symmetric=True, condense=True)
    aFC += Variation( NeoHookeC(mu,lam,C,Log=True)*dx ).Compile()
    aFC += Variation( InnerProduct(C - CF,Sig)*dx ).Compile()
    aFC += Variation( InnerProduct(Fsym-Grad(u)-I,Psym)*dx ).Compile()
    aFC += Variation( (u-uh)*n*(Psym*n)*n*dx(element_boundary=True) ).Compile()
    aFC += Variation( -par*force*u.Trace()*ds("right") )

    #Set F=I, C=I
    gfu_FC.components[2].Set(I)
    gfu_FC.components[4].Set(I)
    
    with TaskManager():
        for step in range(numsteps):
            print("step = ", step)
            par.Set( (step+1)/numsteps )
            (err, nit) = solvers.Newton(aFC, gfu_FC, dampfactor=0.1, maxit=20, printing=False, maxerr=5e-8, inverse="sparsecholesky")

    return { "displacement" : gfu_FC.components[0], "err" : err, "nit" : nit }


def Solve(method, mesh, order):
    result = None
    if method == "std":
        try:
            result = StandardMethod(mesh, order)
        except:
            print("Exception in standard method was thrown!")
    elif method == "F":
        try:
            result = TDNNS_F_Method(mesh, order)
        except:
            print("Exception in F method was thrown!")
    elif method == "C":
        try:
            result = TDNNS_C_Method(mesh, order)
        except:
            print("Exception in C method was thrown!")
    elif method == "FC":
        try:
            result = TDNNS_FC_Method(mesh, order)
        except:
            print("Exception in FC method was thrown!")
    else:
        raise Exception("method ", method, "not implemented!")
    return result


results = []


for f in [8,16,24,32]:
    print("force = ", f)
    fz.Set(f)
    results.append([])
    
    for nx in [2,4,8,16]:
        print("Grid = ", nx, "x", nx)
        mesh = MakeQuadMesh(nx=nx, ny=nx, mapping = lambda x,y: (L_1*x,L_2*x+L_2*y-(L_2-L_3)*x*y) )
        Draw(mesh)
        results[-1].append([])

        for method in ["std", "F", "C", "FC"]:
            result = Solve(method,mesh,order=2)
            U = sqrt(Integrate( result["displacement"]*result["displacement"], mesh))
            u_A = result["displacement"](mesh(P[0],P[1]))
            print("u(a)",method," = ", u_A)
            print("||u||_L2",method," = ", U)
            results[-1][-1].append( (u_A[1], U) )

            Draw(Norm(result["displacement"]), mesh, "abs_u_"+method)
            Draw(result["displacement"], mesh, "u_"+method)

        input("Press enter to continue")

print(results)
